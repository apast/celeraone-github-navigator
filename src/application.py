from concurrent.futures import ThreadPoolExecutor
from operator import itemgetter
from tornado.concurrent import return_future
from tornado.gen import coroutine
from tornado.ioloop import IOLoop
from tornado.queues import Queue
from tornado.web import Application, RequestHandler
from urllib import request, parse
from urllib.error import HTTPError
import argparse
import gzip
import json
import logging
import multiprocessing
import os
import tornado


LOG = logging.getLogger(__name__)


class GitNavigator():
    def __init__(self, api_base="https://api.github.com",
                 client_id=None,
                 client_secret=None,
                 max_threads=5):
        self.max_threads = max_threads
        self.api_base = api_base
        self.client_id = client_id
        self.client_secret = client_secret

    def get_last_commit_multi(self, projects_tuples):
        results = []

        with ThreadPoolExecutor(max_workers=self.max_threads) as executor:
            def callee(project):
                return self.get_last_commit(project[0], project[1])

            results = executor.map(callee, projects_tuples)

        return results

    def get_last_commit(self, owner, project):
        p = dict(owner=owner, project=project)
        commit_path = "/repos/%(owner)s/%(project)s/commits/master" % p
        commit = self._get(commit_path)
        commit["project"] = project
        commit["owner"] = owner
        return commit

    def search(self, search_term, page=1, per_page=5, sort_field="created_at",
               reverse=False):
        search_path = "/search/repositories"

        params = dict(q=search_term,
                      page=page,
                      per_page=per_page)

        result = self._get(search_path, params)["items"]

        if sort_field:
            result = self._sort_search(result, sort_field, reverse)

        return result

    def _sort_search(self, result, sort_field, reverse=False):
        return sorted(result, key=itemgetter("created_at"), reverse=reverse)

    def _get(self, path, params=None):
        params = params if params else {}

        if self.client_id and self.client_secret:
            params["client_id"] = self.client_id
            params["client_secret"] = self.client_secret

        url = "%s%s?%s" % (self.api_base, path, parse.urlencode(params))

        req = request.Request(url, headers={"Accept-Encoding": "gzip"})

        LOG.info("Requesting GitHub API: %s",
                 url.replace(self.client_id, "*******")
                    .replace(self.client_secret, "******") if self.client_id else url
                 )

        try:
            response = request.urlopen(req)

            try:
                result = gzip.GzipFile(fileobj=response).read()
            except OSError:
                result = response.read()

            result = json.loads(result.decode("utf-8"))
        except HTTPError as e:
            msg = "Check if your IP address achieved the request Rate Limits over GitHub API. More info: https://developer.github.com/v3/search/#rate-limit and use CLI parameters for setup your App ID and Secret"
            LOG.error(msg)
            raise e

        return result


class SearchHandler(RequestHandler):

    def initialize(self, *args, **kwargs):
        self.api = kwargs["api"]
        cwd = os.path.dirname(os.path.realpath(__file__))
        self.template_loader = tornado.template.Loader(cwd)

    def _sort_order(self, value):
        return value if value in ["desc", "asc"] else "desc"

    def _sort_field(self, value):
        return value if value in ["created_at", "forks", "forks_count",
                                  "open_issues", "open_issues_count",
                                  "pushed_at", "score", "size",
                                  "stargazers_count", "updated_at",
                                  "watchers",
                                  "watchers_count"] else "created_at"

    @coroutine
    def get(self):
        search_term = self.get_argument("search_term", "")
        format = self.get_argument("format", "html")
        include_commits = self.get_argument("include_commits", "1") == "1"
        sort_field = self._sort_field(self.get_argument("sort", "created_at"))
        reverse = self.get_argument("reverse", "1") in ["1", "true"]

        if search_term:
            result_json = yield self._search(search_term, sort_field=sort_field,
                                             reverse=reverse)

            if include_commits:
                self._apply_last_commits(result_json)
        else:
            result_json = []

        if format == "json":
            response_body = json.dumps(result_json)
        else:
            response_body = self._render("template.html",
                                         search_term=search_term,
                                         projects=result_json)
        self.write(response_body)
        self.flush()
        self.finish()

    @coroutine
    def _apply_last_commits(self, projects):
        request_map = dict(map(lambda p: ((p["owner"]["login"], p["name"]), p),
                               projects))
        result = yield self._load_last_commit_multi(request_map.keys())

        for commit in result:
            project = request_map[(commit["owner"], commit["project"])]
            project["lastcommit"] = commit

    @return_future
    def _load_last_commit_multi(self, projects, callback=None):
        callback(self.api.get_last_commit_multi(projects))

    @return_future
    def _search(self, search_term, sort_field=None, reverse=True,
                callback=None):
        result = self.api.search(search_term=search_term,
                                 sort_field=sort_field,
                                 reverse=reverse)
        callback(result)

    def _render(self, template, **data):
        return self.template_loader.load(template).generate(**data)


class GitNavigatorWeb():

    def __init__(self, api, port=9876):
        self.port = port
        self.api = api

    def build_app(self, debug=False):
        return Application([
                ("^/navigator/?", SearchHandler, dict(api=self.api))
            ],
            debug=debug)

    def start(self, debug=False):
        app = self.build_app(debug)

        app.listen(self.port)

        LOG.info("Listening on port %s", self.port)

        IOLoop.current().start()


class GitNavigatorCLI():

    def start(self, argv):
        parser = argparse.ArgumentParser()

        self._build_web_parser(parser)

        input_args = parser.parse_args(argv)

        self._start_web_server(input_args)

    def _start_web_server(self, input_args):
        api = self._get_api(input_args)
        navigator_web = GitNavigatorWeb(api, port=input_args.port)
        navigator_web.start(debug=input_args.debug)

    def _get_api(self, input_args):
        return GitNavigator(client_id=input_args.github_clientid,
                            client_secret=input_args.github_secret)

    def _build_web_parser(self, web_parser):
        web_parser.add_argument("--github-secret",
                                type=str,
                                required=False,
                                default=None,
                                help="GitHub client secret",
                                dest="github_secret")

        web_parser.add_argument("--github-id",
                                type=str,
                                required=False,
                                default=None,
                                help="GitHub client ID.",
                                dest="github_clientid")

        web_parser.add_argument("-D", "--debug",
                                action="store_true",
                                default=False,
                                dest="debug")

        web_parser.add_argument("-p", "--port",
                                dest="port",
                                default=9876,
                                type=int)

        web_parser.set_defaults(mode="web")


import unittest


class GitNavigatorTestCase(unittest.TestCase):
    def test_sort_search_results(self):
        curr_path = os.path.dirname(os.path.realpath(__file__))
        cwd = os.path.join(curr_path, "test", "search-result.json")
        with open(cwd, "r") as fp:
            unsorted_result = json.load(fp)["items"]
            self.assertEqual(["tornado",
                              "sockjs-tornado",
                              "tornadio2",
                              "Introduction-to-Tornado",
                              "tornado-celery"],
                             list(map(lambda a: a["name"], unsorted_result)))

            sorted_result = GitNavigator()._sort_search(unsorted_result,
                                                        "created_at")

            sorted_expected = ["tornado",
                               "tornadio2",
                               "Introduction-to-Tornado",
                               "sockjs-tornado",
                               "tornado-celery"]

            self.assertEqual(sorted_expected,
                             list(map(lambda a: a["name"], sorted_result)))

            sorted_desc_result = GitNavigator()._sort_search(unsorted_result,
                                                             "created_at",
                                                             reverse=True)

            sorted_expected.reverse()
            self.assertEqual(sorted_expected,
                             list(map(lambda a: a["name"],
                                      sorted_desc_result)))
            sorted_expected.reverse()


if __name__ == "__main__":
    import sys
    logging.basicConfig(level=logging.INFO)
    LOG = logging.getLogger(__name__)
    GitNavigatorCLI().start(sys.argv[1:])
