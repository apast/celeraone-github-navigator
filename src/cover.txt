=============================
Design & Implementation Notes
=============================

Following task requisites, I wrote a simple application, with reduced
dependency of external library and resources, focusing on evaluation of
concepts and coding style, not in how import libraries and easily do everything
in a single call.

With this principle, built-in python modules taken place, like 'urllib' for HTTP calls over GitHub API support 'and concurrent.futures' module for parallel HTTP requests over
multiple git repositories commits endpoints, reducing delivering time of response for final user.

Tornado framework was used for HTTP requests handling, with concurrent/@coroutine support and embbeded template support.

For production environments, this implementation should be modified and it
would preferable use more robust and modern libraries, like requests, pyzmq or
another msg queue lib/broker and jinja2, for HTTP external calls,
scalability, parallelism, resilience and other concurrence features, and
output templating.

Application CLI was implemented using 'argparser'. Considering GitHub API Rate
Limits for requests, if necessary, command line command supports --github-id and
--github-secret parameters for requests limit increasing.

I used python 3.5 for development and test.


=================
Environment Setup
=================

Install & Run Server


-- Using 'virtualenv'

1. Create a work directory.
  i.e.: mkdir /opt/andrepastore

2. Save or move downloaded file into this directory:
  mv andrepastore-celeraone-githubnav.tar.gz /opt/andrepastore

3. Decompress project file using tar:
  tar -xzf andrepastore-celeraone-githubnav.tar.gz

4. Prepare system environment
  # install virtual environment (using apt, if you use apt.)
  sudo apt-get install virtualenvwrapper

  # create a virtualenv using python3
  virtualenv -p /usr/bin/python3 /opt/andrepastore/venv

  # activate virtual environment
  . /opt/andrepastore/venv/bin/activate

  # install tornado dependency
  pip install tornado

  # run the application
  python application.py


  # If you prefer use github application id and secret to increase your Rate
Limit (https://developer.github.com/v3/search/#rate-limit), you can run the
following command:
  python application.py --github-id <client_id> --github-secret <client_secret>

5. Open your preferrable web browser and visit following link:
  http://localhost:9876/navigator

  i.e:
  http://localhost:9876/navigator?search_term=
  http://localhost:9876/navigator?search_term=tornado


-- Alternative (bitbucket clone, docker, docker-compose)
There is a more complete version using docker and docker-compose. It is
published on BitBucket.

You can download see more info at: https://bitbucket.org/apast/celeraone-github-navigator
Or, clone directly:
git clone https://bitbucket.org/apast/celeraone-github-navigator.git


Any questions and request for improvements, contact me at apastores@gmail.com.

Kind Regards,

Andre Pastore
