FROM python:3

VOLUME /opt/gitnav

WORKDIR /opt/gitnav

COPY requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt
RUN rm /tmp/requirements.txt

ENTRYPOINT python -m application
